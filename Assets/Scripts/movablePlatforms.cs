﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movablePlatforms : MonoBehaviour {

    public bool isVertical;

    public float minHeight;
    public float maxHeight;

    public float minWidth;
    public float maxWidth;

    private Vector3 forwardVec;
    public float speed;

    private bool inMove;
    public GameObject lever;

	// Use this for initialization
	void Start ()
    {
		if (isVertical)
            forwardVec = new Vector3(0.0f, 1.0f, 0.0f);
        else
            forwardVec = new Vector3(1.0f, 0.0f, 0.0f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        checkActivation();

        if (inMove)
        {
            if (isVertical)
            {
                VerticalMovement();
            }
            else
            {
                //HorizontalMovement();
            }
        }
	}

    private void VerticalMovement()
    {
        transform.position += forwardVec * speed * Time.deltaTime;

        if (transform.position.y > maxHeight)
            forwardVec *= -1;
        else if (transform.position.y < minHeight)
            forwardVec *= -1;
    }

    private void checkActivation()
    {
        if (lever.GetComponent<Animator>().enabled)
            inMove = true;
    }
}
