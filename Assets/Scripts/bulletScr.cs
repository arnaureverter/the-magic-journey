﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScr : MonoBehaviour {
    private int dir=1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (dir == 0)
        {
            transform.position = new Vector3(transform.position.x - 15 * Time.deltaTime, transform.position.y, transform.position.z);
        }
        else {
            transform.position = new Vector3(transform.position.x + 15 * Time.deltaTime, transform.position.y, transform.position.z);
        }
	}
    void OnCollisionEnter(Collision collision) {

        if (collision.gameObject.tag == "button")
        {
            Debug.Log("ButtonPressed");
            collision.gameObject.GetComponent<interruptor>().doorToOpen.GetComponent<movablePlatforms>().enabled = true;
            
        }

        else
        {
            Destroy(this.gameObject);
        }
    }
}
