﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuroSuelo : MonoBehaviour {

    public BoxCollider myCollider;
    public float speed;
    public float height;
    private bool goodCollider=false;
    private float finalPosY;

    private void Start()
    {
        this.gameObject.transform.localScale = new Vector3 (this.gameObject.transform.localScale.x, height, this.gameObject.transform.localScale.z);

        finalPosY = this.gameObject.transform.position.y + height/2.0f;
    }

    void Update ()
    {
        transform.position = Vector3.MoveTowards(this.gameObject.transform.position, new Vector3(this.gameObject.transform.position.x, finalPosY, this.gameObject.transform.position.z), speed * Time.deltaTime);
        if (!goodCollider && transform.position.y == finalPosY)
            goodColliderFunct();
        
	}

    void goodColliderFunct()
    {
        myCollider.size = new Vector3(1f, 0.2f, 1f);
        myCollider.center = new Vector3(0f, 0.4f, 0f);
        goodCollider = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            Destroy(collision.gameObject);
        }
              

    }
}
