﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tornadoVertex : MonoBehaviour {

    private GameObject PullOBJ;
    private Rigidbody PullOBJRigid;

    public float pullSpeed;
    public float reboundSpeed;
    
    public void OnTriggerEnter(Collider coll)
    {
        Debug.Log(transform.up);

        if (coll is MeshCollider)
        {
            if (coll.gameObject.tag == "Cage" || coll.gameObject.tag == "Player")
            {
                PullOBJ = coll.gameObject;
                PullOBJRigid = PullOBJ.GetComponent<Rigidbody>();

                PullOBJRigid.AddForce(transform.up * reboundSpeed, ForceMode.Impulse);
            }
        }
    }

    public void OnTriggerStay(Collider coll)
    {
        if (coll is MeshCollider)
        {
            if (coll.gameObject.tag == "Cage" || coll.gameObject.tag == "Player")
            {
                PullOBJ = coll.gameObject;
                PullOBJRigid = PullOBJ.GetComponent<Rigidbody>();

                PullOBJRigid.AddForce(transform.up * pullSpeed, ForceMode.Force);
            }
        }

        if (coll.gameObject.tag == "Muro")
        {
            this.gameObject.GetComponent<BoxCollider>().size -= new Vector3 (0,Time.deltaTime*reboundSpeed,0);
            this.gameObject.GetComponent<BoxCollider>().center -= new Vector3(0, Time.deltaTime * reboundSpeed, 0);
        }
    }
}
