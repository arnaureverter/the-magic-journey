﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    private Vector3 position;
    private Vector3 move = new Vector3(3, 0, 0);
    public bool beginLeft = false;
    public bool beginUp = false;
    public bool beginDown=false;
    private int max = 10;
    private int min = 0;
    public float speed;
	// Use this for initialization
	void Start () {
        if (beginLeft)
        {
            move *= -1;
            max = 0;
            min = -5;
        }
        if (beginUp)
        {
            move = new Vector3(0, 3, 0);
        }
        else if (beginDown)
        {
            move = move = new Vector3(0, -3, 0);
        }
            position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if(beginUp||beginDown)
        {
            
            transform.position = Vector3.MoveTowards(transform.position, transform.position + move, speed * Time.deltaTime);

            if (transform.position.y > position.y + max)
                move *= -1;
            if (transform.position.y < position.y + min)
                move *= -1;
        }
        else{
            transform.position = Vector3.MoveTowards(transform.position, transform.position + move, speed * Time.deltaTime);

            if (transform.position.x > position.x + max)
                move *= -1;
            if (transform.position.x < position.x + min)
                move *= -1;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            move *= -1;
           
        }
    }
}
