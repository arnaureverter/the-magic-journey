﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCrates : MonoBehaviour {
    private Vector3 position;
    public GameObject caja;
	// Use this for initialization
	void Start () {

        position = this.gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Spikes")
        {
            transform.position = position;
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

}
