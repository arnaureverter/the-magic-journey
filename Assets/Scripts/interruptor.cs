﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interruptor : MonoBehaviour {

    public GameObject doorToOpen;

    private bool isOpening = false;
    private float openingSpeed = 3.0f;

    private Renderer doorRenderer;
    private Vector3 finalDoorPos;

    // Use this for initialization
    void Start ()
    {
        doorRenderer = doorToOpen.GetComponent<Renderer>();

        finalDoorPos = doorToOpen.transform.position - new Vector3(0.0f, doorRenderer.bounds.size.y, 0.0f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isOpening)
            OpenDoor();
        if (doorToOpen.transform.position == finalDoorPos)
            isOpening = false;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                isOpening = true;
            }
        }
    }

    private void OpenDoor()
    {
        doorToOpen.transform.position = Vector3.MoveTowards(doorToOpen.transform.position, finalDoorPos, openingSpeed * Time.deltaTime);
    }
}
