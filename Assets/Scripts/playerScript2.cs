﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript2 : MonoBehaviour {

    private int activeAbility = 1;
    
    public Vector3 startPosition;

    public float iniSpeed;
    private float speed = 8.0f;
    public float jumpSpeed;
    public Rigidbody myRigidbody;
	bool suelo;

    private bool boxGlued = false;
    private GameObject movingBox;
    private bool isLeft;

    private bool slowed = false;
    private Ray pulsacion;
    private RaycastHit colision;

    public GameObject wall;
    private GameObject actualWall;
    public GameObject bulletG;
    public static string ActualAbility = "Detener tiempo";
    private bool canJump;
    private float contBullet;

    private void Start()
    {   
        startPosition = transform.position;
    }

    private void Update()
    {
        contBullet += Time.deltaTime;

        CheckAbility();
        if (Input.GetMouseButton(1)) {
            if (contBullet > 0.5f)
            {
               contBullet = 0;
               GameObject l_bullet= Instantiate(bulletG,transform.position+new Vector3(0.75f,0f),Quaternion.identity);
                
            }
        }
       

        if (boxGlued && movingBox != null)
        {
            speed = 3f;
            MovingBoxes();
        }
        else
            speed = iniSpeed;


        if (activeAbility == 1)
            SlowTime();
        if (activeAbility == 2)
            GenerateWall();

    }

    private void FixedUpdate()
    {
        
        MovementPlayer();
    }

    private void MovementPlayer()
    {
        
        
        if (canJump)
        {
            if (Input.GetKey(KeyCode.W))
            {
               
                canJump = false;
                myRigidbody.AddForce(transform.up * jumpSpeed);
               
            }
           speed = 8;
        }
        else
        {
            if (myRigidbody.velocity.magnitude > 12) {
                myRigidbody.velocity = myRigidbody.velocity.normalized * 12;
            }
            speed = 6;
            if (myRigidbody.velocity.y < 0)
            {
                Physics.gravity = new Vector3(0, -22f, 0);
            }
            else
            {
                Physics.gravity = new Vector3(0, -20f, 0);
            }



        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Water" || other.gameObject.tag == "Enemy" || other.gameObject.tag == "Spikes")
        {
            Restart(startPosition);
        }
        if (other.gameObject.tag == "Respawn")
        {
            startPosition = other.gameObject.transform.position;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "lever")
            if (Input.GetKeyDown(KeyCode.E))
                other.gameObject.GetComponent<Animator>().enabled = true;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Suelo" || collision.gameObject.tag == "Muro" || collision.gameObject.tag == "Cage" || collision.gameObject.tag == "plataformaMovil" || collision.gameObject.tag == "SueloCaja")
        {
            canJump = true;
			suelo = true;
			if (collision.gameObject.tag == "plataformaMovil") {
				this.transform.parent = collision.transform;
			} else {
				this.transform.parent = null;
			}
        }

        if (collision.collider is BoxCollider)
        {
            
            if (collision.gameObject.tag == "Cage")
            {
                Debug.Log("left");
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    isLeft = true;
                    movingBox = collision.gameObject;
                    boxGlued = true;
                }
                else
                    boxGlued = false;
            }
            
        }
        if (collision.collider is SphereCollider)
        {
            Debug.Log("right");

            if (collision.gameObject.tag == "Cage")
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    isLeft = false;
                    movingBox = collision.gameObject;
                    boxGlued = true;
                }
                else
                    boxGlued = false;
            }
        }

     }
  
    //  //  //  //  //  //  //  //  //  //  //

    private void CheckAbility()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            activeAbility = 1;
            ActualAbility = "Detener tiempo";
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            activeAbility = 2;
            ActualAbility = "Generar muros";
        }

    }

    //  //  //  //  //  //  //  //  //  //  //

    private void GenerateWall()
    {
        if (Input.GetMouseButtonDown(0))
        {
            pulsacion = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(pulsacion, out colision))
            {
                if (colision.rigidbody.tag == "Suelo" || colision.rigidbody.tag == "SueloCaja")
                {
                    if (actualWall != null)
                        Destroy(actualWall);

                    actualWall = Instantiate(wall, new Vector3(colision.point.x, colision.transform.position.y, colision.transform.position.z), Quaternion.identity);
                }
            }
            activeAbility = 0;
        }
      

    }

    //  //  //  //  //  //  //  //  //  //  //

    private void MovingBoxes()
    {
        if (isLeft)
            movingBox.transform.position = new Vector3(transform.position.x + 2f, movingBox.transform.position.y, movingBox.transform.position.z);
        else
            movingBox.transform.position = new Vector3(transform.position.x - 2f, movingBox.transform.position.y, movingBox.transform.position.z);
    }

    //  //  //  //  //  //  //  //  //  //  //

    private void SlowTime()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!slowed)
            {
                Time.timeScale = 0.1f;
                slowed = true;
            }
            else
            {
                Time.timeScale = 1;
                slowed = false;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            pulsacion = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(pulsacion, out colision))
            {
                if (colision.rigidbody)
                {
                    if (!colision.rigidbody.isKinematic && colision.rigidbody.tag == "Cage")
                    {
                        Debug.Log("allahu akbar");

                        colision.rigidbody.isKinematic = true;
                        colision.rigidbody.gameObject.tag = "SueloCaja";
                        slowed = false;
                        Time.timeScale = 1;
                    }
                    else if (colision.rigidbody.isKinematic && colision.rigidbody.tag == "SueloCaja")
                    {
                        colision.rigidbody.isKinematic = false;
                        colision.rigidbody.gameObject.tag = "Cage";
                    }
                }
            }
            activeAbility = 0;
        }
        
    }

    public void Restart(Vector3 startPosition)
    {
        transform.position = startPosition;
    }
}
